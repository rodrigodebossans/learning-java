package com.taskTwo.dev.main.questionThree.service;

import com.taskTwo.dev.main.questionThree.controller.CandidateController;
import com.taskTwo.dev.main.questionThree.model.Menu;
import com.taskTwo.dev.main.questionThree.util.Util;


import java.util.Scanner;

public class MenuService {

    public MenuService() {
    }

    private void writeHeader() {
        Util util = new Util();

        util.writeln("\n|\tMENU PRINCIPAL DA QUESTAO 3-------|\n");
        util.writeln("|\t1 -> Inserir candidato            |");
        util.writeln("|\t2 -> Votar                        |");
        util.writeln("|\t3 -> Placar de votos              |");
        util.writeln("|\t4 -> Resultado da eleiçao         |");
        util.writeln("|\t0 -> Sair                         |\n");
        util.writeln("|\tRodrigo Debossans-----------------|");
        util.write("\n|\tDigite sua opção: ");
    }

    public void menu() {
        Menu menu = new Menu();
        Scanner input = new Scanner(System.in);
        CandidateController candidateController = new CandidateController();

        do {
            this.writeHeader();
            menu.setOption(input.nextInt());
            switch(menu.getOption()) {
                case 1: candidateController.addCandidate(); break;
                case 2: candidateController.vote(); break;
                case 3: candidateController.voteScore(); break;
                case 4: candidateController.electionResult(); break;
                case 0: break;
            }
        } while (menu.getOption() != 0);
        
    }
}


