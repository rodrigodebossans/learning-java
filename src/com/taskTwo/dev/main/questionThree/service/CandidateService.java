package com.taskTwo.dev.main.questionThree.service;

import com.taskTwo.dev.main.questionThree.model.Candidate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CandidateService {

    private List<Candidate> candidateList = new ArrayList<Candidate>();

    public boolean addCandidate(Candidate candidate) {
        return this.candidateList.add(candidate);
    }

    public Candidate getCandidateByNumber(Integer number) {
        return this.candidateList.stream().filter(candidate -> candidate.getNumber().equals(number)).findAny().orElse(null);
    }

    public boolean checksCandidateExistenceByNumber(Integer number) {
        return this.candidateList.stream().anyMatch(candidate -> candidate.getNumber().equals(number));
    }

    public boolean updateCandidateByNumber(Integer number, Candidate candidate) {
        if(!this.checksCandidateExistenceByNumber(number))
            return false;

        this.candidateList.set(this.candidateList.indexOf(candidate), candidate);
        return true;
    }

    public boolean voteByCandidateNumber(Integer number) {
        Candidate candidate = this.getCandidateByNumber(number);
        if(candidate == null) return false;
        candidate.setNumberOfVotes(candidate.getNumberOfVotes() + 1);
        return this.updateCandidateByNumber(number, candidate);
    }

    public List<Candidate> getCandidateLIst() {
        return this.candidateList;
    }

    public Candidate getWinnerCandidate() {
        Candidate winnerCandidate = new Candidate(this.candidateList.get(0));
        this.candidateList.stream().forEach(currentCandidate -> {
            if(currentCandidate.getNumberOfVotes() > winnerCandidate.getNumberOfVotes()) {
                winnerCandidate.setNumber(currentCandidate.getNumber());
                winnerCandidate.setName(currentCandidate.getName());
                winnerCandidate.setNumberOfVotes(currentCandidate.getNumberOfVotes());
            }
        });
        return winnerCandidate;
    }

    public Integer getTotalofVotes() {
        AtomicReference<Integer> totalVotes = new AtomicReference<>(0);
        this.candidateList.stream().forEach(candidate -> {
            totalVotes.set(totalVotes.get() + candidate.getNumberOfVotes());
        });
        return totalVotes.get();
    }

    public Integer calculatePercentageOfVotesByCandidate(Candidate candidate) {
        Integer totalOfVotes = this.getTotalofVotes();

        if(candidate.getNumberOfVotes() > totalOfVotes)
            return null;

        return ((100 * candidate.getNumberOfVotes()) / totalOfVotes);
    }

    public List<Candidate> getElectionReportList() {
        this.candidateList.stream()
                .forEach(
                        candidate -> {
                            candidate.setPercentageVotes(this.calculatePercentageOfVotesByCandidate(candidate));
                        });
        return this.candidateList;
    }
}
