package com.taskTwo.dev.main.questionThree.controller;

import com.taskTwo.dev.main.questionOne.util.Util;
import com.taskTwo.dev.main.questionThree.model.Candidate;
import com.taskTwo.dev.main.questionThree.service.CandidateService;

import java.util.List;
import java.util.Scanner;

public class CandidateController {

    private Util util = new Util();
    private Scanner input = new Scanner(System.in);

    private CandidateService candidateService = new CandidateService();

    public CandidateController() {
    }

    public void addCandidate() {
        this.util.write("\n|\t\tDigite um numero para o candidato: ");
        Integer candidateNumber = this.input.nextInt();

        this.util.write("|\t\tDigite um nome para o candidato: ");
        String candidateName = this.input.next();

        Candidate candidate = new Candidate(candidateNumber, candidateName);

        if(this.candidateService.checksCandidateExistenceByNumber(candidateNumber)){
            this.util.writeln("\n|\t-> Candidato(a) ja esta cadastrado!");
            return;
        }


        if(!this.candidateService.addCandidate(candidate)) {
            this.util.writeln("\n|\t-> Erro ao adicionar o candidato(a)  " + candidateName + ", tente novamente.");
            return;
        }

        this.util.writeln("\n|\t> Candidato(a) " + candidateName + " foi criado com sucesso!");
    }

    public void vote() {
        this.util.write("\n|\t\tDigite um numero do candidato que deseja votar: ");
        Integer candidateNumber = this.input.nextInt();

        if(!this.candidateService.voteByCandidateNumber(candidateNumber)) {
            this.util.writeln("\n|\t-> Erro ao ao votar, tente novamente.");
            return;
        }

        this.util.writeln("\n|\t> Voto realizado com sucesso!");
    }

    public void voteScore() {
        List<Candidate> candidateList = this.candidateService.getCandidateLIst();
        util.writeln("\n|\t-> Placar de votos:");
        candidateList.stream()
            .forEach(
                candidate -> {
                    util.writeln("\n|\t\t-> Numero: " + candidate.getNumber());
                    util.writeln("|\t\t-> Nome: " + candidate.getName());
                    util.writeln("|\t\t-> Numero de votos: " + candidate.getNumberOfVotes());
                });
    }

    public void electionResult() {
        Candidate winnerCandidate = new Candidate(this.candidateService.getWinnerCandidate());

        util.writeln("\n|\t-> Candidato(a) vencedor:");
        util.writeln("\n|\t\t-> Numero: " + winnerCandidate.getNumber());
        util.writeln("|\t\t-> Nome: " + winnerCandidate.getName());
        util.writeln("|\t\t-> Numero de votos: " + winnerCandidate.getNumberOfVotes());

        util.writeln("\n|\t-> Relatorio da eleiçao:");

        List<Candidate> electionReportList = this.candidateService.getElectionReportList();

        util.writeln("\n|\t\t-> Total de votos realizados: " + this.candidateService.getTotalofVotes());

        electionReportList.stream()
                .forEach(
                        candidate -> {
                            util.writeln("\n|\t\t\t-> Numero: " + candidate.getNumber());
                            util.writeln("|\t\t\t-> Nome: " + candidate.getName());
                            util.writeln("|\t\t\t-> Total de votos: " + candidate.getNumberOfVotes());
                            util.writeln("|\t\t\t-> Percentual de votos: " + candidate.getPercentageVotes() + "%");
                        });
    }
}


