package com.taskTwo.dev.main.questionThree.model;

public class Menu {
    private int option;

    public Menu() {
    }

    public Menu(int option) {
        this.option = option;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }
}
