package com.taskTwo.dev.main.questionThree.model;

public class Candidate {
    private Integer number;
    private String name;
    private Integer numberOfVotes;
    private Integer percentageVotes;

    public Candidate() {
    }

    public Candidate(Integer number, String name) {
        this.number = number;
        this.name = name;
        this.numberOfVotes = 0;
        this.percentageVotes = 0;
    }

    public Candidate(Candidate candidate) {
        this.number = candidate.getNumber();
        this.name = candidate.getName();
        this.numberOfVotes = candidate.getNumberOfVotes();
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(Integer numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }

    public Integer getPercentageVotes() {
        return percentageVotes;
    }

    public void setPercentageVotes(Integer percentageVotes) {
        this.percentageVotes = percentageVotes;
    }
}
