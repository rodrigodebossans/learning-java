package com.taskTwo.dev.main.questionThree;

import com.taskTwo.dev.main.questionThree.controller.MenuController;

public class App {

    public static void main(String[] args) {
        MenuController menuController = new MenuController();
        menuController.menu();
    }
}