package com.taskTwo.dev.main.questionTwo;

import com.taskTwo.dev.main.questionTwo.controller.MenuController;

public class App {

    public static void main(String[] args) {
        MenuController menuController = new MenuController();
        menuController.menu();
    }
}