package com.taskTwo.dev.main.questionTwo.controller;

import com.taskTwo.dev.main.questionOne.util.Util;
import com.taskTwo.dev.main.questionTwo.model.Account;
import com.taskTwo.dev.main.questionTwo.model.RurchaseRecord;
import com.taskTwo.dev.main.questionTwo.service.AccountService;

import java.util.Date;
import java.util.Scanner;

public class AccountController {

    private Util util = new Util();
    private Scanner input = new Scanner(System.in);

    private AccountService accountService = new AccountService();


    public AccountController() {
    }

    public void addAccount() {
        Account account = new Account();

        this.util.write("\n|\t\tDigite o numero da conta: ");
        account.setNumber(input.nextInt());

        this.util.write("|\t\tDigite o saldo da conta: ");
        account.setBalance(input.nextDouble());

        if (this.accountService.checkAccountExistenceByNumber(account.getNumber())) {
            this.util.writeln("\n|\t-> A conta " + account.getNumber() + "ja existe!");
            return;
        }

        if(!this.accountService.addAccount(account))
            this.util.writeln("\n|\t-> Erro ao criar conta " + account.getNumber());

        this.util.writeln("\n|\t> A conta de numero " + account.getNumber() + " foi criada com sucesso!");
    }

    public void removeAccount() {
        Account account = new Account();

        this.util.write("\n|\t\tDigite o numero da conta que deseja remover: ");
        account.setNumber(this.input.nextInt());

        if (!this.accountService.removeAccountByAccuntNumber(account.getNumber())) {
            this.util.writeln("\n|\t-> Erro ao deletar conta " + account.getNumber());
            return;
        }

        this.util.writeln("\n|\t-> A conta de numero " + account.getNumber() + " foi deletada com sucesso!");
    }

    public void listAccounts() {
        this.accountService.listAccounts();
    }

    public void listBalanceByAccount() {
        Account account = new Account();

        this.util.write("\n|\t\tDigite o numero da conta que deseja obter o saldo: ");
        account.setNumber(this.input.nextInt());

        account.setBalance(this.accountService.getBalanceByAccount(account));

        if (account.getBalance() == null) {
            this.util.writeln("\n|\t-> Erro ao listar saldo da conta " + account.getNumber());
            return;
        }

        this.util.writeln("\n|\t-> Saldo da conta " + account.getNumber() + " e de: " + account.getBalance());
    }

    public void performTransfer() {
        this.util.write("\n|\t\tDigite o numero da conta remetente: ");
        Integer accountSenderNumber = this.input.nextInt();

        this.util.write("|\t\tDigite o numero da conta destinatario: ");
        Integer accountRecipientNumber = this.input.nextInt();

        this.util.write("|\t\tDigite o valor a ser transferido: ");
        Integer transferAmount = this.input.nextInt();

        if (!this.accountService.transferBalance(accountSenderNumber, accountRecipientNumber, transferAmount)) {
            this.util.writeln(
                "\n|\t-> Erro ao transferir "
                        + transferAmount
                        + " reais da conta "
                        + accountSenderNumber
                        + " para a conta "
                        + accountRecipientNumber);
            return;
        }
        this.util.writeln("\n|\t-> Transferencia realizada com sucesso!");


    }

    public void purchase() {
        this.util.write("\n|\t\tDigite o numero da conta para efetuar uma compra: ");
        Integer number = this.input.nextInt();

        Account account = this.accountService.getAccountByNumber(number);

        if(account == null) {
            this.util.writeln("\n|\t-> Erro ao encontrar conta " + number);
            return;
        }

        this.util.write("|\t\tDigite o valor da compra: ");
        Double purchasePrice = this.input.nextDouble();

        if(purchasePrice <= 0 || purchasePrice > account.getBalance()) {
            this.util.writeln("\n|\t-> Erro: valor invalido / saldo insuficiente");
            return;
        }

        this.util.writeln("\n|\tEscolha o metodo de pagamento: ");
        this.util.writeln("|\t\t1 -> Credito");
        this.util.writeln("|\t\t2 -> Debito");
        this.util.writeln("|\t\t0 -> Cancelar");

        this.util.write("\n|\tDigite sua opção: ");
        Integer option = this.input.nextInt();

        switch (option) {
            case 1:
                this.util.write("\n|\t\tDigite a quantidade de parcelas: ");
                Integer qtInstallments = this.input.nextInt();
                RurchaseRecord rurchaseRecordCredit = new RurchaseRecord(new Date(), purchasePrice, false, true, qtInstallments);
                account.setCreditLimit(account.getCreditLimit() - purchasePrice);
                if(!account.addShoppingRecords(rurchaseRecordCredit) || !this.accountService.updateAccountByNumber(number, account)) {
                    this.util.writeln("\n|\t-> Erro, tente novamente");
                    return;
                }
                this.util.writeln("\n|\t-> Compra efetuada com sucesso!");
                break;
            case 2:
                RurchaseRecord rurchaseRecordDebit = new RurchaseRecord(new Date(), purchasePrice, true, false, 0);
                account.setBalance(account.getBalance() - purchasePrice);
                if(!account.addShoppingRecords(rurchaseRecordDebit) || !this.accountService.updateAccountByNumber(number, account)) {
                    this.util.writeln("\n|\t-> Erro, tente novamente");
                    return;
                }
                this.util.writeln("\n|\t-> Compra efetuada com sucesso!");
                break;
            case 0: return;
        }

    }

}
