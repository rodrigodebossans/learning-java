package com.taskTwo.dev.main.questionTwo.model;

import java.util.ArrayList;
import java.util.List;

public class Account {
    private Integer number;
    private Double balance;
    private Double creditLimit = 3000.0;
    private List<RurchaseRecord> shoppingRecords = new ArrayList<RurchaseRecord>();

    public Account() {
    }

    public Account(Integer number, Double balance) {
        this.number = number;
        this.balance = balance;
    }

    public Account(Integer number, Double balance, List<RurchaseRecord> shoppingRecords) {
        this.number = number;
        this.balance = balance;
        this.shoppingRecords = shoppingRecords;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public List<RurchaseRecord> getShoppingRecords() {
        return shoppingRecords;
    }

    public void setShoppingRecords(List<RurchaseRecord> shoppingRecords) {
        this.shoppingRecords = shoppingRecords;
    }

    public Double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public boolean addShoppingRecords(RurchaseRecord rurchaseRecord) {
        return this.shoppingRecords.add(rurchaseRecord);
    }
}
