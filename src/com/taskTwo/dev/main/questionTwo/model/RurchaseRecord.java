package com.taskTwo.dev.main.questionTwo.model;

import java.util.Date;

public class RurchaseRecord {
    private Date date;
    private Double price;
    private boolean debit;
    private boolean credit;
    private Integer qtInstallments;

    public RurchaseRecord() {
    }

    public RurchaseRecord(Date date, Double price, boolean debit, boolean credit, Integer qtInstallments) {
        this.date = date;
        this.price = price;
        this.debit = debit;
        this.credit = credit;
        this.qtInstallments = qtInstallments;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isDebit() {
        return debit;
    }

    public void setDebit(boolean debit) {
        this.debit = debit;
    }

    public boolean isCredit() {
        return credit;
    }

    public void setCredit(boolean credit) {
        this.credit = credit;
    }

    public Integer getQtInstallments() {
        return qtInstallments;
    }

    public void setQtInstallments(Integer qtInstallments) {
        this.qtInstallments = qtInstallments;
    }

    public String getPaymentMethod(){
        return (this.isCredit()) ? "Credito" : "Debito";
    }
}
