package com.taskTwo.dev.main.questionTwo.service;

import com.taskTwo.dev.main.questionTwo.model.Menu;
import com.taskTwo.dev.main.questionTwo.util.Util;
import com.taskTwo.dev.main.questionTwo.controller.AccountController;


import java.util.Scanner;

public class MenuService {

    public MenuService() {
    }

    private void writeHeader() {
        Util util = new Util();

        util.writeln("\n|\tMENU PRINCIPAL DA QUESTAO 2-------|\n");
        util.writeln("|\t1 -> Inserir conta                |");
        util.writeln("|\t2 -> Listar contas                |");
        util.writeln("|\t3 -> Consultar saldo              |");
        util.writeln("|\t4 -> Comprar                      |");
        util.writeln("|\t5 -> Realizar Transferencia       |");
        util.writeln("|\t6 -> Excluir conta                |");
        util.writeln("|\t0 -> Sair                         |\n");
        util.writeln("|\tRodrigo Debossans-----------------|");
        util.write("\n|\tDigite sua opção: ");
    }

    public void menu() {
        Menu menu = new Menu();
        Scanner input = new Scanner(System.in);
        AccountController accountController = new AccountController();

        do {
            this.writeHeader();
            menu.setOption(input.nextInt());
            switch(menu.getOption()) {
                case 1: accountController.addAccount(); break;
                case 2: accountController.listAccounts(); break;
                case 3: accountController.listBalanceByAccount(); break;
                case 4: accountController.purchase(); break;
                case 5: accountController.performTransfer(); break;
                case 6: accountController.removeAccount(); break;
                case 0: break;
            }
        } while (menu.getOption() != 0);
        
    }
}


