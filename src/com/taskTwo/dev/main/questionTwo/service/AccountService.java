package com.taskTwo.dev.main.questionTwo.service;

import com.taskTwo.dev.main.questionOne.util.Util;
import com.taskTwo.dev.main.questionTwo.model.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class AccountService {

    private List<Account> accountList = new ArrayList<Account>();

    public boolean addAccount(Account account) {
        return this.accountList.add(account);
    }

    public void listAccounts() {
        Util util = new Util();
        util.writeln("\n|\t-> Lista de contas existentes: ");
        if (this.accountList.isEmpty()) {
            util.writeln("\n|\t-> Ainda nao existe contas cadastradas.");
        } else {
            this.accountList.stream()
                    .forEach(
                            account -> {
                                util.writeln("\n|\t\t-> Numero: " + account.getNumber());
                                util.writeln("|\t\t-> Saldo: " + account.getBalance());
                                util.writeln("|\t\t-> Limite de Credito: " + account.getCreditLimit());
                                if (!account.getShoppingRecords().isEmpty()) {
                                    account.getShoppingRecords().stream()
                                            .forEach(
                                                    rurchaseRecord -> {
                                                        util.writeln("\n|\t\t\t-> Data da compra: " + rurchaseRecord.getDate().toLocaleString());
                                                        util.writeln("|\t\t\t-> Valor: " + rurchaseRecord.getPrice());
                                                        util.writeln("|\t\t\t-> Metodo de pagamento: " + rurchaseRecord.getPaymentMethod());
                                                        util.writeln("|\t\t\t-> Numero de parcelas: " + rurchaseRecord.getQtInstallments());
                                                    });
                                } else {
                                    util.writeln("|\t\t\t-> Nenhum registro de compra.");
                                }
                            });
        }
    }

    public boolean removeAccountByAccuntNumber(Integer number) {
        Predicate<Account> condition = cAccount -> cAccount.getNumber().equals(number);
        boolean remotionResult = this.accountList.removeIf(condition);
        System.gc();
        return remotionResult;
    }

    public Double getBalanceByAccount(Account account) {
        for (Account accountI : this.accountList) {
            if (accountI.getNumber().equals(account.getNumber())) {
                return accountI.getBalance();
            }
        }
        return null;
    }

    public Account getAccountByNumber(Integer number) {
        return this.accountList.stream().filter(account -> account.getNumber().equals(number)).findAny().orElse(null);
    }

    public boolean checkAccountExistenceByNumber(Integer accountNumber) {
        return this.accountList.stream().anyMatch(account -> account.getNumber().equals(accountNumber));
    }

    public boolean updateAccountByNumber(Integer number, Account account) {
        if(!this.checkAccountExistenceByNumber(number))
            return false;

        this.accountList.set(this.accountList.indexOf(account), account);
        return true;
    }

    public boolean transferBalance(Integer accountSenderNumber, Integer accountRecipientNumber, Integer transferAmount) {
        if (transferAmount <= 0)
            return false;

        if(!this.checkAccountExistenceByNumber(accountSenderNumber))
            return false;

        if(!this.checkAccountExistenceByNumber(accountRecipientNumber))
            return false;

        Account accountSender = this.getAccountByNumber(accountSenderNumber);
        Account accountRecipient = this.getAccountByNumber(accountRecipientNumber);

        if(accountSender == null || accountRecipient == null)
            return false;

        accountSender.setBalance(accountSender.getBalance() - transferAmount);
        accountRecipient.setBalance(accountRecipient.getBalance() + transferAmount);

        if(!this.updateAccountByNumber(accountSender.getNumber(), accountSender) || !this.updateAccountByNumber(accountRecipient.getNumber(), accountRecipient))
            return false;

        return true;
    }

}
