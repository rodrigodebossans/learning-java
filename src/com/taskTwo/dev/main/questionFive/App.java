
package com.taskTwo.dev.main.questionFive;

import com.taskTwo.dev.main.questionFive.controller.MenuController;

public class App {

  public static void main(String[] args) {
    MenuController menuController = new MenuController();
    menuController.menu();
  }

}