package com.taskTwo.dev.main.questionFive.service;

import com.taskTwo.dev.main.questionFive.controller.StudentController;
import com.taskTwo.dev.main.questionFive.model.Menu;
import com.taskTwo.dev.main.questionFive.util.Util;

import java.util.Scanner;

public class MenuService {

  private Scanner input = new Scanner(System.in);

  public MenuService() {
  }

  private void writeHeader() {
    Util util = new Util();

    util.writeln("\n|\tMENU PRINCIPAL DO EXERCICIO 5-----|\n");
    util.writeln("|\t1 -> Inserir novo aluno           |");
    util.writeln("|\t2 -> Excluir Aluno                |");
    util.writeln("|\t3 -> Lançar/editar notas          |");
    util.writeln("|\t4 -> Consultar situaçao do aluno  |");
    util.writeln("|\t5 -> Listar todos                 |\n");
    util.writeln("|\t0 -> Sair                         |\n");
    util.writeln("|\tRodrigo Debossans-----------------|");
    util.write("\n|\tDigite sua opção: ");
  }

  public void menu() {
    Menu menu = new Menu();

    StudentController studentController = new StudentController();

    do {
      this.writeHeader();
      menu.setOption(this.input.nextInt());
      switch(menu.getOption()) {
        case 1: studentController.addStudent(); break;
        case 2: studentController.removeStudent(); break;
        case 3: studentController.editStudent(); break;
        case 4: studentController.consultSituation(); break;
        case 5: studentController.listAll(); break;
        case 0: break;
      }
    } while (menu.getOption() != 0);

  }

  private void writeHeaderNotes() {
    Util util = new Util();

    util.writeln("\n|\tO que voce deseja fazer?");
    util.writeln("\n|\t\t1 -> Adicionar nova nota");
    util.writeln("|\t\t0 -> Sair");
    util.write("\n|\t\tDigite sua opção: ");
  }

  public boolean menuNotes() {
    Menu menuNotes = new Menu();

    do {
      this.writeHeaderNotes();
      menuNotes.setOption(this.input.nextInt());

      if(menuNotes.getOption() == 1) {
        return false;
      }

    } while (menuNotes.getOption() != 0);

    return true;
  }

}

