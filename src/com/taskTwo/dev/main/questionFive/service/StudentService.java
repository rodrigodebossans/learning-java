package com.taskTwo.dev.main.questionFive.service;

import com.taskTwo.dev.main.questionFive.model.Note;
import com.taskTwo.dev.main.questionFive.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentService {

    private List<Student> studentList = new ArrayList<Student>();

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public boolean addStudent(Student student) {
        return studentList.add(student);
    }

    public boolean removeStudentByRegistration(Integer registration) {
        return this.studentList.removeIf(student -> student.getRegistration().equals(registration));
    }

    public Student calculateAverage(Student student) {
        if(this.getStudentByRegistration(student.getRegistration()) == null)
            return null;

        Double totNote = 0.0;

        for (Note note : student.getNoteList()) {
            totNote += note.getValue();
        }

        student.setAverage(totNote / student.getNoteList().size());

        if(student.getAverage() < 7.0) {
            student.setSituation("Reprovado");
        } else {
            student.setSituation("Aprovado");
        }

        return student;
    }

    public Student getStudentByRegistration(Integer registration) {
        return this.studentList.stream()
                .filter(student -> student.getRegistration().equals(registration))
                .findFirst().orElse(null);
    }

    public boolean overwriteStudent(Student student) {

        if(this.getStudentByRegistration(student.getRegistration()) == null)
            return false;

        this.studentList.set(this.studentList.indexOf(student), this.calculateAverage(student));
        return true;
    }


}
