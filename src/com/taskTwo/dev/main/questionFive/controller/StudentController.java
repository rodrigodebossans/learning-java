package com.taskTwo.dev.main.questionFive.controller;

import com.taskTwo.dev.main.questionFive.model.Note;
import com.taskTwo.dev.main.questionFive.model.Student;
import com.taskTwo.dev.main.questionFive.service.MenuService;
import com.taskTwo.dev.main.questionFive.service.StudentService;
import com.taskTwo.dev.main.questionOne.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentController {

    private Util util = new Util();
    private Scanner input = new Scanner(System.in);

    private StudentService studentService = new StudentService();
    private MenuService menuService = new MenuService();

    private void printsStudent(Student student) {
        this.util.writeln("\n|\t\t-> Matricula: " + student.getRegistration());
        this.util.writeln("|\t\t-> Nome: " + student.getName());
        this.util.writeln("|\t\t-> Notas: ");
        if (student.getNoteList().size() > 0) {
            student
                .getNoteList()
                .forEach(
                    note -> {
                        this.util.writeln("\n|\t\t\t-> Descriçao: " + note.getDescription());
                        this.util.writeln("|\t\t\t-> Nota: " + note.getValue());
                    });
        } else {
            this.util.writeln("\n|\t\t\t-> Sem notas lançadas");
        }
        this.util.writeln("\n|\t\t-> Media: " + student.getAverage());
        this.util.writeln("\n|\t\t-> Situaçao: " + student.getSituation());
    }

    public void addStudent() {
        Student student = new Student();

        this.util.write("\n|\tDigite uma matricula do aluno: ");
        student.setRegistration(this.input.nextInt());

        if(this.studentService.getStudentByRegistration(student.getRegistration()) != null) {
            this.util.writeln("\n|\t-> Erro, matricula existente!");
            return;
        }

        this.util.write("|\tDigite o nome do aluno: ");
        student.setName(this.input.next());

        if(!this.studentService.addStudent(student)) {
            this.util.writeln("\n|\t-> Erro ao adicionar o aluno!");
            return;
        }

        this.util.writeln("\n|\t> Aluno adicionado com sucesso!");
    }

    public void removeStudent() {

        this.util.write("\n|\tDigite a matricula do aluno que deseja excluir: ");
        Integer registration = this.input.nextInt();

        if(this.studentService.getStudentByRegistration(registration) == null) {
            this.util.writeln("\n|\t-> Erro, matricula inexistente!");
            return;
        }

        if(!this.studentService.removeStudentByRegistration(registration)) {
            this.util.writeln("\n|\t-> Erro ao excluir o aluno!");
            return;
        }

        this.util.writeln("\n|\t> Aluno excluido com sucesso!");
    }

    public void listAll() {
        this.util.writeln("\n|\t-> Lista dos alunos cadastrados ");

        this.studentService
            .getStudentList()
            .forEach(
                student -> {
                    this.printsStudent(student);
                });
    }

    public void editStudent() {
        Student student = new Student();

        this.util.write("\n|\tDigite a matricula do aluno que deseja editar: ");
        student.setRegistration(this.input.nextInt());

        student = this.studentService.getStudentByRegistration(student.getRegistration());

        if (student == null) {
            this.util.writeln("\n|\t-> Erro! Aluno nao encontrado.");
            return;
        }

        List<Note> noteList = new ArrayList<Note>();

        do {
            Note note = new Note();

            this.util.write("\n|\tDigite uma descriçao para a nota: ");
            note.setDescription(this.input.next());

            this.util.write("\n|\tDigite a nota: ");
            note.setValue(this.input.nextDouble());

            if(!noteList.add(note)) {
                this.util.writeln("\n|\t-> Erro ao adicionar nota.");
                return;
            }

            this.util.writeln("\n|\t> Nota para " + note.getDescription() + " adicionada com sucesso!");

        } while(!this.menuService.menuNotes());

        student.setNoteList(noteList);

        if(!this.studentService.overwriteStudent(student)){
            this.util.writeln("\n|\t-> Erro ao editar aluno.");
            return;
        }

        this.util.writeln("\n|\t-> Aluno editado com sucesso!");

    }

    public void consultSituation() {
        this.util.write("\n|\tDigite a matricula do aluno que deseja consultar: ");
        Student student = this.studentService.getStudentByRegistration(this.input.nextInt());

        if(student == null) {
            this.util.writeln("\n|\t-> Erro, aluno nao encontrado!");
            return;
        }

        this.printsStudent(student);
    }

}
