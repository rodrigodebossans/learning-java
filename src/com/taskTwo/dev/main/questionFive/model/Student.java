package com.taskTwo.dev.main.questionFive.model;

import java.util.ArrayList;
import java.util.List;

public class Student {
  private Integer registration;
  private String name;
  private List<Note> noteList = new ArrayList<Note>();
  private Double average;
  private String situation;

  public Student() {
    this.situation = "Indefinido";
    this.average = 0.0;
  }

  public Student(Student student) {
    this.registration = student.getRegistration();
    this.name = student.getName();
    this.noteList = student.getNoteList();
    this.average = student.getAverage();
    this.situation = student.getSituation();
  }

  public Integer getRegistration() {
    return registration;
  }

  public void setRegistration(Integer registration) {
    this.registration = registration;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Note> getNoteList() {
    return noteList;
  }

  public void setNoteList(List<Note> noteList) {
    this.noteList = noteList;
  }

  public Double getAverage() {
    return average;
  }

  public void setAverage(Double average) {
    this.average = average;
  }

  public String getSituation() {
    return situation;
  }

  public void setSituation(String situation) {
    this.situation = situation;
  }

  public boolean addNote(Note note) {
    return this.noteList.add(note);
  }

  public boolean removeNote(Note note) {
    return this.noteList.removeIf(tmpNote -> tmpNote.equals(note));
  }
}
