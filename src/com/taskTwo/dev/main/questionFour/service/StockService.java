package com.taskTwo.dev.main.questionFour.service;

import com.taskTwo.dev.main.questionFour.model.Product;
import com.taskTwo.dev.main.questionTwo.model.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class StockService {

    private List<Product> productList = new ArrayList<Product>();

    public Product getProductByName(String name) {
        return this.productList.stream()
                .filter(product -> product.getName().equals(name))
                .findAny()
                .orElse(null);
    }

    public boolean addProduct(Product product) {
        return this.productList.add(product);
    }

    public List<Product> getReplacementProducts() {
        List<Product> replacementProducts = new ArrayList<Product>();

        this.productList.stream()
                .filter(product -> product.getQuantityInStock() < 5)
                .forEach(replacementProducts::add);

        return replacementProducts;
    }

    public boolean removeProduct(String name) {
        return this.productList.removeIf(product -> product.getName().equalsIgnoreCase(name));
    }

    public Double getInventoryBalance() {
        Double inventoryBalance = 0.0;
        for (Product product : this.productList) {
            inventoryBalance += (product.getPrice() * product.getQuantityInStock());
        }
        return inventoryBalance;
    }
}
