package com.taskTwo.dev.main.questionFour.service;

import com.taskTwo.dev.main.questionFour.controller.StockController;
import com.taskTwo.dev.main.questionFour.model.Menu;
import com.taskTwo.dev.main.questionFour.util.Util;

import java.util.Scanner;

public class MenuService {

    public MenuService() {
    }

    private void writeHeader() {
        Util util = new Util();

        util.writeln("\n|\tMENU PRINCIPAL DO EXERCICIO 4-----|\n");
        util.writeln("|\t1 -> Adicionar produto            |");
        util.writeln("|\t2 -> Verificar reposiçao          |");
        util.writeln("|\t3 -> Retirar produto do estoque   |");
        util.writeln("|\t4 -> Listar saldo do estoque      |");
        util.writeln("|\t0 -> Sair                         |\n");
        util.writeln("|\tRodrigo Debossans-----------------|");
        util.write("\n|\tDigite sua opção: ");
    }

    public void menu() {
        Menu menu = new Menu();
        Scanner input = new Scanner(System.in);
        StockController stockController = new StockController();

        do {
            this.writeHeader();
            menu.setOption(input.nextInt());
            switch(menu.getOption()) {
                case 1: stockController.addProduct(); break;
                case 2: stockController.checkProductReplacement(); break;
                case 3: stockController.removeProductFromStock(); break;
                case 4: stockController.listInventoryBalance(); break;
                case 0: break;
            }
        } while (menu.getOption() != 0);

    }

}

