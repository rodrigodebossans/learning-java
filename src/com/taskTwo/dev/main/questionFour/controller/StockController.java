package com.taskTwo.dev.main.questionFour.controller;

import com.taskTwo.dev.main.questionFour.model.Product;
import com.taskTwo.dev.main.questionFour.service.StockService;
import com.taskTwo.dev.main.questionOne.util.Util;

import java.util.Scanner;

public class StockController {

    private Util util = new Util();
    private Scanner input = new Scanner(System.in);

    private StockService stockService = new StockService();

    public void addProduct() {
        Product product = new Product();

        this.util.write("\n|\tDigite o nome do produto: ");
        product.setName(this.input.next());

        this.util.write("|\tDigite o preço do produto: ");
        product.setPrice(this.input.nextDouble());

        this.util.write("|\tDigite a quantidade em estoque: ");
        product.setQuantityInStock(this.input.nextInt());

        if(!this.stockService.addProduct(product)) {
            this.util.writeln("\n|\t-> Erro ao adicionar o produto!");
            return;
        }

        this.util.writeln("\n|\t> Produto adicionado com sucesso!");
    }

    public void checkProductReplacement() {
        this.util.writeln("\n|\t-> Lista dos produtos que precisam ser repostos (quantidade < 5) ");

        this.stockService.getReplacementProducts()
                .forEach(productReplacement -> {
                    this.util.writeln("\n|\t\t-> Nome: " + productReplacement.getName());
                    this.util.writeln("|\t\t-> Preço: " + productReplacement.getPrice());
                    this.util.writeln("|\t\t-> Quantidade em estoque: " + productReplacement.getQuantityInStock());
                });
    }

    public void removeProductFromStock() {
        this.util.write("\n|\tDigite o nome do produto que deseja remover: ");
        String name = this.input.next();

        if(!this.stockService.removeProduct(name)) {
            this.util.writeln("\n|\t-> Erro ao remover o produto!");
            return;
        }

        this.util.writeln("\n|\t-> Produto removido com sucesso!");
    }

    public void listInventoryBalance() {
        this.util.writeln("\n|\t-> Quantia em Real dos produtos contidos no estoque");
        this.util.writeln("\n|\t\t-> Valor total: " + this.stockService.getInventoryBalance());
    }

}
