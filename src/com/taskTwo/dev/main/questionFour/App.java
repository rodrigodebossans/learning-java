
package com.taskTwo.dev.main.questionFour;

import com.taskTwo.dev.main.questionFour.controller.MenuController;

public class App {

    public static void main(String[] args) {
        MenuController menuController = new MenuController();
        menuController.menu();
    }

}