package com.taskTwo.dev.main.questionFour.util;

public class Util {

    public Util() {
    }

    public void write(String message) {
        System.out.print(message);
    }

    public void writeln(String message) {
        System.out.println(message);
    }
}
