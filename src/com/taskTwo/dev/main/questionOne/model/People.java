package com.taskTwo.dev.main.questionOne.model;

import java.util.ArrayList;
import java.util.List;

public class People {
    private String name;
    private Long identity;

    private List<People> peopleList = new ArrayList<People>();

    public People() {
    }

    public People(String name, long identity) {
        this.name = name;
        this.identity = identity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIdentity() {
        return identity;
    }

    public void setIdentity(long identity) {
        this.identity = identity;
    }

    public List<People> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<People> peopleList) {
        this.peopleList = peopleList;
    }
}
