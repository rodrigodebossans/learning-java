package com.taskTwo.dev.main.questionOne.controller;

import com.taskTwo.dev.main.questionOne.model.People;
import com.taskTwo.dev.main.questionOne.service.PeopleService;
import com.taskTwo.dev.main.questionOne.util.Util;

import java.util.Scanner;

public class PeopleController {

    private PeopleService peopleService = new PeopleService();

    public PeopleController() {
    }

    public void addPeople() {
        Util util = new Util();
        People people = new People();
        Scanner input = new Scanner(System.in);

        util.write("|\t\t-> Digite um nome: ");
        people.setName(input.next());

        util.write("|\t\t-> Digite a identidade: ");
        people.setIdentity(input.nextLong());

        if (!this.peopleService.addPeople(people))
            util.writeln("\n|\t\t-> Erro ao adicionar " + people.getName());

        util.writeln("\n|\t\t-> " + people.getName() + " foi adicionado com sucesso!");
    }

    public void listPeoples(){
        this.peopleService.listPeoples();
    }
}
