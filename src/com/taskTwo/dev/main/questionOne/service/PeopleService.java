package com.taskTwo.dev.main.questionOne.service;

import com.taskTwo.dev.main.questionOne.model.People;
import com.taskTwo.dev.main.questionOne.util.Util;

import java.util.ArrayList;
import java.util.List;

public class PeopleService {

    private List<People> listPeoples = new ArrayList<People>();

    public boolean addPeople(People people) {
        return this.listPeoples.add(people);
    }

    public void listPeoples() {
        Util util = new Util();

        util.writeln("\n|\tPessoas cadastradas---------------|");
        this.listPeoples.stream()
                .forEach(
                        people -> {
                            util.writeln("\n|\t\t-> Nome: " + people.getName());
                            util.writeln("|\t\t-> Identidade: " + people.getIdentity());
                        });
    }
}
