package com.taskTwo.dev.main.questionOne.service;

import java.util.Scanner;

import com.taskTwo.dev.main.questionOne.controller.PeopleController;
import com.taskTwo.dev.main.questionOne.model.Menu;
import com.taskTwo.dev.main.questionOne.util.Util;

public class MenuService {

    public MenuService() {
    }

    private void writeHeader() {
        Util util = new Util();

        util.writeln("\n|\tMENU PRINCIPAL DO EXERCICIO 1-----|\n");
        util.writeln("|\t1 -> Inserir pessoa               |");
        util.writeln("|\t2 -> Listar pessoas               |");
        util.writeln("|\t0 -> Sair                         |\n");
        util.writeln("|\tRodrigo Debossans-----------------|");
        util.write("\n|\tDigite sua opção: ");
    }

    public void menu() {
        Menu menu = new Menu();
        Scanner input = new Scanner(System.in);
        PeopleController peopleController = new PeopleController();

        do {
            this.writeHeader();
            menu.setOption(input.nextInt());
            switch(menu.getOption()) {
                case 1: peopleController.addPeople(); break;
                case 2: peopleController.listPeoples(); break;
                case 0: break;
            }
        } while (menu.getOption() != 0);
        
    }
}
