package com.taskTwo.dev.main.questionOne;

import com.taskTwo.dev.main.questionOne.controller.MenuController;

public class App {

    public static void main(String[] args) {
        MenuController menuController = new MenuController();
        menuController.menu();
    }

}