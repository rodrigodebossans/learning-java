package com.av1.dev.main.service;

import com.av1.dev.main.model.People;

import java.util.ArrayList;
import java.util.List;

public class CachePeopleService {

    List<People> cachedPeople = new ArrayList<People>();

    public boolean insertCachedPeopleIfNotExists(People people) {
        if(!this.cachedPeople.contains(people))
            return this.cachedPeople.add(people);

        return false;
    }

    public People returnPeople(Integer id) {
        return this.cachedPeople.stream().filter(people -> people.getId().equals(id)).findAny().orElse(null);
    }
}
