package com.av1.dev.main.service;

import com.av1.dev.main.model.People;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PeopleService {


    private List<People> peopleList = new ArrayList<People>();
    private CachePeopleService cachedPeopleService = new CachePeopleService();

    private Integer _countId = 1;
    private List<Integer> _cachedPeoplesId = new ArrayList<Integer>();

    private boolean verifyCachePeopleId(Integer id) {
        Integer idPeopleCached = this._cachedPeoplesId
            .stream()
            .filter(cacheId -> cacheId.equals(id))
            .findFirst()
            .orElse(null);

        if(idPeopleCached == null)
            return false;

        return true;
    }

    public void insertPeoples() {
        People _joao = new People(this._countId++, "Joao", 10);
        People _alice = new People(this._countId++, "Alice", 5);
        People _fernando = new People(this._countId++, "Fernando", 27);
        People _carlos = new People(this._countId++, "Carlos", 12);
        People _priscila = new People(this._countId++, "Priscila", 31);

        this.peopleList.add(_joao);
        this.peopleList.add(_alice);
        this.peopleList.add(_fernando);
        this.peopleList.add(_carlos);
        this.peopleList.add(_priscila);
    }

    public List<People> getPeoplesSortedByAge() {
        List<People> tmpPeopleList = this.peopleList;

        Collections.sort(tmpPeopleList, new Comparator<People>() {
            @Override
            public int compare(People p1, People p2) {
                return p1.getAge() - p2.getAge();
            }
        });

        return tmpPeopleList;
    }

    public People getPeopleById(Integer id) {

        if(this.verifyCachePeopleId(id))
            return this.cachedPeopleService.returnPeople(id);

        People foundPerson = this.peopleList
            .stream()
            .filter(tmpPeople -> tmpPeople.getId().equals(id))
            .findAny()
            .orElse(null);

        if (foundPerson != null) {
            this._cachedPeoplesId.add(id);
            this.cachedPeopleService.insertCachedPeopleIfNotExists(foundPerson);
        }

        return foundPerson;
    }
}
