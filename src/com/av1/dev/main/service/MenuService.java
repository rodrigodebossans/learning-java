package com.av1.dev.main.service;

    import com.av1.dev.main.controller.PeopleController;
    import com.av1.dev.main.model.Menu;
    import com.av1.dev.main.util.Util;

    import java.util.Scanner;

public class MenuService {

    public MenuService() {
    }

    private void writeHeader() {
        Util util = new Util();

        util.writeln("\n|\tMENU PRINCIPAL DA AV1-------------|\n");
        util.writeln("|\t1 -> Listar Pessoas               |");
        util.writeln("|\t2 -> Consultar pessoa             |");
        util.writeln("|\t0 -> Sair                         |\n");
        util.writeln("|\tRodrigo Debossans-----------------|");
        util.write("\n|\tDigite sua opção: ");
    }

    public void menu() {
        Menu menu = new Menu();
        Scanner input = new Scanner(System.in);
        PeopleController peopleController = new PeopleController();

        peopleController.initialize();

        do {
            this.writeHeader();
            menu.setOption(input.nextInt());
            switch(menu.getOption()) {
                case 1: peopleController.listAll(); break;
                case 2: peopleController.consultPerson(); break;
                case 0: break;
            }
        } while (menu.getOption() != 0);

    }
}



