package com.av1.dev.main.util;

public class Util {

    public Util() {
    }

    public void write(String message) {
        System.out.print(message);
    }

    public void writeln(String message) {
        System.out.println(message);
    }
}
