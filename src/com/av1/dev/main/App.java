package com.av1.dev.main;

import com.av1.dev.main.controller.MenuController;

public class App {
    public static void main(String[] args) {
        MenuController menuController = new MenuController();
        menuController.menu();
    }
}
