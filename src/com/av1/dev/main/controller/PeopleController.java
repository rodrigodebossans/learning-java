package com.av1.dev.main.controller;

import com.av1.dev.main.model.People;
import com.av1.dev.main.service.PeopleService;
import com.av1.dev.main.util.Util;

import java.util.List;
import java.util.Scanner;

public class PeopleController {

    private Util util = new Util();
    private Scanner input = new Scanner(System.in);

    private PeopleService peopleService = new PeopleService();

    public void initialize() {
        this.peopleService.insertPeoples();
    }

    public void listAll() {
        this.util.writeln("\n\tPessoas ordenadas por idade: ");

        List<People> peopleListFromPrint = this.peopleService.getPeoplesSortedByAge();

        if(peopleListFromPrint == null) {
            this.util.writeln("\n|\t-> Erro, tente novamente mais tarde!");
            return;
        }

        peopleListFromPrint
            .forEach(
                people -> {
                    this.util.writeln("\n\t\tId -> " + people.getId());
                    this.util.writeln("\t\tNome -> " + people.getName());
                    this.util.writeln("\t\tIdade -> " + people.getAge());
                });
    }

    public void consultPerson() {
        this.util.write("\n|\t Digite o Id da pessoa que deseja consultar: ");
        Integer id = this.input.nextInt();

        People people = this.peopleService.getPeopleById(id);

        if(people == null) {
            this.util.writeln("\n|\t-> Pessoa nao encontrada, tente novamente mais tarde!");
            return;
        }

        this.util.writeln("\n\t\tId: " + people.getId());
        this.util.writeln("\t\tNome: " + people.getName());
        this.util.writeln("\t\tIdade: " + people.getAge());
    }

}
