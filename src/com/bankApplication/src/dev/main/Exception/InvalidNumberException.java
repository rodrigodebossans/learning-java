package dev.main.Exception;

public class InvalidNumberException extends GeneralException {
    
    public InvalidNumberException() {
        super("Dados invalidos, informe numeros positivos diferentes de 0.");
    }
    
}
