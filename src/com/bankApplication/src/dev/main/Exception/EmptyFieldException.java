package dev.main.Exception;

public class EmptyFieldException extends GeneralException {
    
    public EmptyFieldException() {
        super("Formulario invalido, preencha todos campos.");
    }
    
}
