package dev.main.Exception;

public class GeneralException extends Exception {
    public GeneralException(String message) {
        super(message);
    }
}
