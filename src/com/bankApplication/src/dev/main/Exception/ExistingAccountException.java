package dev.main.Exception;

import dev.main.Model.Account.BankAccount;

public class ExistingAccountException extends GeneralException {
    
    public ExistingAccountException(BankAccount account) {
        super("A conta " + account.getAccountNumber() + " ja existe.");
    }
    
}
