package dev.main.Exception;

import dev.main.Model.Account.BankAccount;

public class NotFoundAccountException extends GeneralException {
    
    public NotFoundAccountException(Long accountNumber) {
        super("A conta " + accountNumber + " nao foi encontrada.");
    }
    
}

