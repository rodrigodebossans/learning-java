package dev.main.Exception;

public class InsufficientFundsException extends GeneralException {
    
    public InsufficientFundsException() {
        super("Saldo insuficiente");
    }
    
}
