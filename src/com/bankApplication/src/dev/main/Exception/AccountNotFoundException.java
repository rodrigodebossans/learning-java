package dev.main.Exception;

public class AccountNotFoundException extends GeneralException {
    
    public AccountNotFoundException(Long accountNumber) {
        super("A conta de numero" + accountNumber + "nao foi encontrada.");
    }
    
}
