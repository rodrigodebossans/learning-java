package dev.main.Service.Account;

import dev.main.Exception.EmptyFieldException;
import dev.main.Exception.ExistingAccountException;
import dev.main.Exception.InsufficientFundsException;
import dev.main.Exception.NotFoundAccountException;
import dev.main.Exception.InvalidNumberException;
import dev.main.Model.Account.BankAccount;
import dev.main.Model.Account.ChainAccount;
import dev.main.Model.Account.SavingsAccount;
import dev.main.Model.Bank.Bank;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AccountService {
    
    public AccountService() {
    }
    
    public static void addAccount(BankAccount account) throws ExistingAccountException {
        if (Bank.searchAccount(account.getAccountNumber()) == null) {
            Bank.insertAccount(account);
            JOptionPane.showMessageDialog(null, "Conta cadastrada com sucesso!");
        } else
            throw new ExistingAccountException(account);
    }
    
    public static void removeAccount(Long accountNumber) throws NotFoundAccountException {
        if (Bank.searchAccount(accountNumber) == null) {
            throw new NotFoundAccountException(accountNumber);
        } else
            Bank.removeAccount(accountNumber);
            JOptionPane.showMessageDialog(null, "Conta removida com sucesso!");
    }
    
    public static void deposit(BankAccount account, Double valueToDeposit) {
        ChainAccount chainAccount = null;
        SavingsAccount savingsAccount = null;
        NumberFormat df = DecimalFormat.getCurrencyInstance();

        if (account instanceof ChainAccount) {
           chainAccount = (ChainAccount) account;
           chainAccount.deposit(valueToDeposit);
           JOptionPane.showMessageDialog(null, "Deposito de "+
                 df.format(valueToDeposit) + " efetuado com sucesso na conta " +
                chainAccount.getAccountNumber() + ".");
        }
        else if (account instanceof SavingsAccount) {
            savingsAccount = (SavingsAccount) account;
            savingsAccount.deposit(valueToDeposit);
            JOptionPane.showMessageDialog(null, "Deposito de "+
                df.format(valueToDeposit) + " efetuado com sucesso na conta " +
                savingsAccount.getAccountNumber() + ".");
        }
    }
    
    public static void withdraw(BankAccount account, Double valueToWithdraw)
    throws InsufficientFundsException {
        ChainAccount chainAccount = null;
        SavingsAccount savingsAccount = null;
        NumberFormat df = DecimalFormat.getCurrencyInstance();

        if (account instanceof ChainAccount) {
           chainAccount = (ChainAccount) account;
           chainAccount.withdraw(valueToWithdraw);
           JOptionPane.showMessageDialog(null, "Saque de "+
                 df.format(valueToWithdraw) + " efetuado com sucesso na conta " +
                chainAccount.getAccountNumber() + ".");
        }
        else if (account instanceof SavingsAccount) {
            savingsAccount = (SavingsAccount) account;
            savingsAccount.withdraw(valueToWithdraw);
            JOptionPane.showMessageDialog(null, "Saque de "+
                df.format(valueToWithdraw) + " efetuado com sucesso na conta " +
                savingsAccount.getAccountNumber() + ".");
        }       
    }
    
    public static void transfer(Double value, BankAccount accountOrigin, Long accountNumberDestiny)
    throws NotFoundAccountException, InsufficientFundsException {
        BankAccount accountDestiny = Bank.searchAccount(accountNumberDestiny);
        if(accountDestiny == null) throw new NotFoundAccountException(accountNumberDestiny);
        accountOrigin.transfer(value, accountOrigin, accountDestiny);
    }
    
    public static void validateToInsert(JTextField inputAccountNumber,
            JTextField inputBalance,
            JTextField inputRateOrLimit)
            throws InvalidNumberException, EmptyFieldException {
        if(
            inputAccountNumber.getText().isEmpty() ||
            inputBalance.getText().isEmpty() ||
            inputRateOrLimit.getText().isEmpty()
        )
            throw new EmptyFieldException();

        if(
            Long.parseLong(inputAccountNumber.getText()) <= 0 ||
            Double.parseDouble(inputBalance.getText()) <= 0 ||
            Double.parseDouble(inputRateOrLimit.getText()) <= 0
        )
            throw new InvalidNumberException();
    }
    
    public static void validateToRemove(JTextField inputSearchRemoveAccount)
    throws InvalidNumberException, EmptyFieldException {
        if(inputSearchRemoveAccount.getText().isEmpty())
            throw new EmptyFieldException();
        
        if(Long.parseLong(inputSearchRemoveAccount.getText()) <= 0)
            throw new InvalidNumberException();
    }
    
    public static void validateAccountNumberField(JTextField inputAccountNumber)
    throws InvalidNumberException, EmptyFieldException {
        if(inputAccountNumber.getText().isEmpty())
            throw new EmptyFieldException();
        
        if(Long.parseLong(inputAccountNumber.getText()) <= 0)
            throw new InvalidNumberException();
    }
    
    public static void validateMovementValue(JTextField inputValue)
    throws InvalidNumberException, EmptyFieldException {
        if(inputValue.getText().isEmpty())
            throw new EmptyFieldException();
        
        if(Long.parseLong(inputValue.getText()) <= 0)
            throw new InvalidNumberException();
    }
}
