package dev.main.Model.Bank;

import dev.main.Model.Account.BankAccount;
import java.util.ArrayList;
import java.util.function.Predicate;

public class Bank {
    private static ArrayList<BankAccount> accounts = new ArrayList();

    public static ArrayList getAccounts() {
        return accounts;
    }

    public static void setAccounts(ArrayList accounts) {
        Bank.accounts = accounts;
    }
    
    public static void insertAccount(BankAccount account) {
        Bank.accounts.add(account);
    }

    public static void removeAccount(Long accountNumber) {
        Predicate<BankAccount> p = (account) -> account.getAccountNumber().equals(accountNumber);
        Bank.accounts.removeIf(p);
    }

    public static BankAccount searchAccount(Long accountNumber) {
        Predicate<BankAccount> p = (account) -> account.getAccountNumber().equals(accountNumber);
        return (BankAccount) Bank.accounts.stream().filter(p).findAny().orElse(null);
    }
    
}

