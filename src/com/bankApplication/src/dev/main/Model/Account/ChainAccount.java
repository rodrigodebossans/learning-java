package dev.main.Model.Account;

import dev.main.Exception.InsufficientFundsException;
import dev.main.Interface.Printable;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ChainAccount extends BankAccount implements Printable {

    public ChainAccount() { }

    private Double limit;

    public ChainAccount(Double limit) {
        this.limit = limit;
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }

    @Override
    public void withdraw(Double value) throws InsufficientFundsException {
        
        if ((this.getBalance() < value && this.getLimit() < value) &&
                (this.getBalance() + this.getLimit()) < value)
             throw new InsufficientFundsException();
        
        if (this.getBalance() >= value) {
            this.setBalance(this.getBalance() - value);
        }
        else if (this.getLimit() >= value && this.getBalance() <= 0) {
            this.setLimit(this.getLimit() - value);
        } else {
           // valorSaque = 6000 / saldo = 2000, limite = 5000
            Double withdrawFromBalance = 0.0;
            Double withdrawFromLimit = 0.0;
            
//            if (this.getBalance() > this.getLimit()) {
//                withdrawFromLimit = this.getLimit();
//                withdrawFromBalance = value - this.getLimit();
//            } else {
                withdrawFromBalance = this.getBalance();
                withdrawFromLimit = value - this.getBalance();               
//            }
            
            this.setBalance(this.getBalance() - withdrawFromBalance);
            this.setLimit(this.getLimit() - withdrawFromLimit);
        }
    };

    @Override
    public void deposit(Double value) {
        this.setBalance(this.getBalance() + value);
    };

    @Override
    public String showData() {
        NumberFormat df = DecimalFormat.getCurrencyInstance();
        return "\n\n| Numero: "+ this.getAccountNumber() +"\n"
                + "| Tipo: Corrente\n" 
                + "| Saldo: " + df.format(this.getBalance()) + "\n" 
                + "| Limite: " + df.format(this.getLimit());
    }
    
    
    @Override
    public String toString() {
        return "Conta: " + this.getAccountNumber();
    }
}