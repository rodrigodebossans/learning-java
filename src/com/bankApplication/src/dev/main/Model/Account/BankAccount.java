package dev.main.Model.Account;

import dev.main.Exception.InsufficientFundsException;
import dev.main.Service.Account.AccountService;

public abstract class BankAccount {
  private Long accountNumber;
  private Double balance;

  public Long getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(Long accountNumber) {
    this.accountNumber = accountNumber;
  }

  public Double getBalance() {
    return balance;
  }

  public void setBalance(Double balance) {
    this.balance = balance;
  }

  public abstract void withdraw(Double value) throws InsufficientFundsException;

  public abstract void deposit(Double value);

  public void transfer(Double value, BankAccount accountOrigin, BankAccount accountDestiny) throws InsufficientFundsException {
      AccountService.withdraw(accountOrigin, value);
      AccountService.deposit(accountDestiny, value);
  }
}