package dev.main.Model.Account;

import dev.main.Exception.InsufficientFundsException;
import dev.main.Interface.Printable;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class SavingsAccount extends BankAccount implements Printable {
    private Double operationFee;

    public SavingsAccount() {
    }

    public Double getOperationFee() {
        return operationFee;
    }

    public void setOperationFee(Double operationFee) {
        this.operationFee = operationFee;
    }

    @Override
    public void withdraw(Double value) throws InsufficientFundsException {
        if (this.getBalance() >= (value + this.operationFee))
            this.setBalance((this.getBalance() - value) - this.operationFee);
        else
            throw new InsufficientFundsException();
    };

    @Override
    public void deposit(Double value) {
        this.setBalance(this.getBalance() + value);
    };

    @Override
    public String showData() {
        NumberFormat df = DecimalFormat.getCurrencyInstance();
        return "\n\n| Numero: "+ this.getAccountNumber() +"\n"
                + "| Tipo: Poupança\n" 
                + "| Saldo: " + df.format(this.getBalance()) + "\n" 
                + "| Taxa: " + df.format(this.getOperationFee());
    }
    
    @Override
    public String toString() {
        return "Conta: " + this.getAccountNumber();
    }
}
