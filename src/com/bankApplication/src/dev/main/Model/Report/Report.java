package dev.main.Model.Report;

import dev.main.Interface.Printable;

public class Report {
    public static String generateReport(Printable object) {
        return object.showData();
    }
}