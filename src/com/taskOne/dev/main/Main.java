package com.taskOne.dev.main;

import com.taskOne.dev.main.entitys.App;

public class Main {

    public static void main(String[] args) {
        App app = new App();
        app.start();
    }
}
