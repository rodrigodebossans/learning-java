package com.taskOne.dev.main.entitys;

public class Climate {

    private double tempFahrenheit;
    private double tempCelsius;

    public Climate() {
    }

    public Climate(double tempFahrenheit) {
        this.tempFahrenheit = tempFahrenheit;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }

    public void setTempFahrenheit(double tempFahrenheit) {
        this.tempFahrenheit = tempFahrenheit;
    }

    public double getTempCelsius() {
        return tempCelsius;
    }

    public void setTempCelsius(double tempCelsius) {
        this.tempCelsius = tempCelsius;
    }

    public double convertFahrenheitToCelcius(double tempFahrenheit) {
        return ((tempFahrenheit - 32) * 5) / 9;
    }

    public double convertCelsiusToFahrenheit(double tempCelsius) {
        return ((tempCelsius * 9 / 5) + 32);
    }

}
