package com.taskOne.dev.main.entitys;

import java.util.Scanner;
import java.lang.Double;

public class App {

    private int option;
    private Scanner input = new Scanner(System.in);

    public App() {
    }

    public App(int option, Scanner input) {
        this.option = option;
        this.input = input;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }

    private void write(String message) {
        System.out.println(message);
    }

    private void writeHeader() {
        this.write("\n|\tMENU PRINCIPAL-SIMPLES (1 AO 7)---|\n");
        this.write("|\t1 -> Corrigir exercicio 1         |");
        this.write("|\t2 -> Corrigir exercicio 2         |");
        this.write("|\t3 -> Corrigir exercicio 3         |");
        this.write("|\t4 -> Corrigir exercicio 4         |");
        this.write("|\t5 -> Corrigir exercicio 5         |");
        this.write("|\t0 -> Sair                         |\n");
        this.write("|\tRodrigo Debossans-----------------|");
        this.write("\n|\tDigite sua opção: ");
    }

    private void exerciseOne() {
        Circle circle = new Circle();

        this.write("Digite o raio do circulo: ");
        circle.setLightning(input.nextDouble());

        circle.setAreaLightning(circle.calculateArea(circle.getLightning()));

        this.write("Area do circulo = " + circle.getAreaLightning());
    }

    private void exerciseTwo() {
        Climate climate = new Climate();

        this.write("Informe a temperatura em Fahrenheit: ");
        climate.setTempFahrenheit(input.nextDouble());

        climate.setTempCelsius(climate.convertFahrenheitToCelcius(climate.getTempFahrenheit()));

        this.write("A temperatura informada convertida em Celsius = "+climate.getTempCelsius());
    }

    private void exerciseThree() {
        Climate climate = new Climate();

        this.write("Informe a temperatura em Celsius: ");
        climate.setTempCelsius(input.nextDouble());

        climate.setTempFahrenheit(climate.convertCelsiusToFahrenheit(climate.getTempCelsius()));

        this.write("A temperatura informada convertida em Fahrenheit = "+climate.getTempFahrenheit());
    }

    private void exerciseFour() {
        Lamp lamp = new Lamp();
        Convenient convenient = new Convenient();

        this.write("Digite a potencia da lampada: ");
        lamp.setPower(input.nextDouble());

        this.write("Digite a largura do comodo: ");
        convenient.setWidth(input.nextDouble());

        this.write("Digite o comprimento do comodo: ");
        convenient.setLength(input.nextDouble());

        convenient.setArea(convenient.calculateArea(convenient.getWidth(), convenient.getLength()));
        convenient.setPowerOfLampsNeeded(convenient.calculateRequiredPowerPerRoom(convenient.getArea()));
        Double totLamps = convenient.calculateNumberOfLampsNeededPerRoom(convenient.getPowerOfLampsNeeded(), lamp.getPower());
        convenient.setNumberOfLampsNeededPerPower(totLamps.intValue());

        this.write("Numero de lampadas necessarias para iluminar esse comodo = " + convenient.getNumberOfLampsNeededPerPower() + "Lampadas");
    }

    private void exerciseFive() {
        Kitchen kitchen = new Kitchen();

        this.write("Digite o comprimento da cozinha: ");
        kitchen.setLength(input.nextDouble());

        this.write ("Digite a largura da cozinha: ");
        kitchen.setWidth(input.nextDouble());

        this.write ("Digite a altura da cozinha: ");
        kitchen.setHeight(input.nextDouble());

        kitchen.setArea(kitchen.calculateArea(kitchen.getLength(), kitchen.getHeight(), kitchen.getWidth()));
        kitchen.setBoxes(kitchen.calculateQuantityOfTileBoxes(kitchen.getArea()));

        this.write("Considerando que cada caixa atende a "
                + kitchen.getAreaByTileBox()
                + " metros quadrados, quantidade de caixas de azulejos para colocar em todas as paredes = "
                + kitchen.getBoxes());
    }

    private void head() {
        do {
            this.writeHeader();
            this.option = input.nextInt();
            switch(this.option){
                case 1: this.exerciseOne(); break;
                case 2: this.exerciseTwo(); break;
                case 3: this.exerciseThree(); break;
                case 4: this.exerciseFour(); break;
                case 5: this.exerciseFive(); break;
                case 0: break;
            }
        } while (this.option != 0);
    }

    public void start() {
        this.head();
    }
}
