package com.taskOne.dev.main.entitys;

import java.lang.Math;

public class Convenient extends Lamp {

    private double width;
    private double length;
    private double area;
    private double powerOfLampsNeeded;
    private int numberOfLampsNeededPerPower;

    public Convenient() {
    }

    public Convenient(double width, double length, double powerOfLampsNeeded, int numberOfLampsNeededPerPower, double area) {
        this.width = width;
        this.length = length;
        this.powerOfLampsNeeded = powerOfLampsNeeded;
        this.numberOfLampsNeededPerPower = numberOfLampsNeededPerPower;
        this.area = area;
    }

    public Convenient(double power, double width, double length, double powerOfLampsNeeded, int numberOfLampsNeededPerPower, double area) {
        super(power);
        this.width = width;
        this.length = length;
        this.powerOfLampsNeeded = powerOfLampsNeeded;
        this.numberOfLampsNeededPerPower = numberOfLampsNeededPerPower;
        this.area = area;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPowerOfLampsNeeded() {
        return powerOfLampsNeeded;
    }

    public void setPowerOfLampsNeeded(double powerOfLampsNeeded) {
        this.powerOfLampsNeeded = powerOfLampsNeeded;
    }

    public int getNumberOfLampsNeededPerPower() {
        return numberOfLampsNeededPerPower;
    }

    public void setNumberOfLampsNeededPerPower(int numberOfLampsNeededPerPower) {
        this.numberOfLampsNeededPerPower = numberOfLampsNeededPerPower;
    }

    public double calculateArea(double width, double length) {
        return width * length;
    }

    public double calculateRequiredPowerPerRoom(double area) {
        return this.getPowerPerSquareMeter() * area;
    }

    public double calculateNumberOfLampsNeededPerRoom(double neededPower, double powerLamp) {
        return Math.ceil(neededPower / powerLamp);
    }
}
