package com.taskOne.dev.main.entitys;

import java.lang.Math;

public class Circle {

    private double lightning;
    private double areaLightning;

    public Circle() {
    }

    public Circle(double lightning) {
        this.lightning = lightning;
    }

    public double getLightning() {
        return this.lightning;
    }

    public void setLightning(double tmpLightning) {
        this.lightning = tmpLightning;
    }

    public double getAreaLightning() {
        return areaLightning;
    }

    public void setAreaLightning(double areaLightning) {
        this.areaLightning = areaLightning;
    }

    private double getPi() {
        return 3.1415926535898;
    }

    public double calculateArea(double tmpLightning) {
        return this.getPi() * Math.pow(tmpLightning, 2);
    }

}