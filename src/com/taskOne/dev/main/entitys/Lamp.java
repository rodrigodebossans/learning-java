package com.taskOne.dev.main.entitys;

public class Lamp {

    private double power;

    public Lamp() {
    }

    public Lamp(double power) {
        this.power = power;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public int getPowerPerSquareMeter() {
        return 18;
    }

}
