package com.taskOne.dev.main.entitys;

public class Kitchen {

    private double width;
    private double height;
    private double length;
    private double area;
    private double boxes;

    public Kitchen() {
    }

    public Kitchen(double length, double width, double height, double area, double boxes) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.area = area;
        this.boxes = boxes;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getBoxes() {
        return boxes;
    }

    public void setBoxes(double boxes) {
        this.boxes = boxes;
    }

    public double getAreaByTileBox() {
        return 1.5;
    }

    public double calculateArea(double length, double height, double width) {
        return ((length * height * 2) + (width * height * 2));
    }

    public double calculateQuantityOfTileBoxes(double area) {
        return area / this.getAreaByTileBox();
    }


}
